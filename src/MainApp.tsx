import React, { Component } from 'react';
import {
    Navigator
} from 'react-native';
import { Home } from "./components/Home";
import { MessageBlock } from "./components/common/MessageBlock";
import { Contact } from "./components/Contact";

export class MainApp extends Component <{}, {}> {
        renderScene (route: any, navigator: any) {
        if (route) {
            return <MessageBlock navigator={navigator} />
        }
        return <Home navigator={navigator} />
    }

    render() {
        return (
            <Navigator configureScene={(route) => Navigator.SceneConfigs.FloatFromBottom}
                       initialRoute={{name: "Home"}}
                       ref="navigator"
                       renderScene={(route: any, navigator) => {
                           switch(route.name) {
                               case "Contact":
                                return <Contact />
                               case "Home":
                                   return <Home navigator={navigator} />
                               case "About":
                                   return <MessageBlock />
                           }
                       }}
                       />
        );
    }
}