import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
const FBSDK = require('react-native-fbsdk');
const {
    LoginButton,
} = FBSDK;

import {MessageBlock} from "./common/MessageBlock";

interface HomeProps {
    navigator: any
}

export const Home = (props: HomeProps) => {
    if (props.navigator) {
    }
    return (
        <View style={styles.container}>
            <View style={styles.innerContainer}>

                <View>
                    <LoginButton
                        publishPermissions={["publish_actions"]}
                        onLoginFinished={
            (error: any, result: any) => {
              if (error) {
                alert("Login failed with error: " + result.error);
              } else if (result.isCancelled) {
                alert("Login was cancelled");
              } else {
                alert("Login was successful with permissions: " + result.grantedPermissions)
              }
            }
          }
                        onLogoutFinished={() => alert("User logged out")}/>
                </View>
            </View>
        </View>
    );
}


const styles: any = StyleSheet.create({
    container: {
        backgroundColor: '#E67E22',
        borderRadius: 5,
        flex: 1,
        justifyContent: 'center',
    },
    innerContainer: {
        backgroundColor: "#B35400",
        height: 50,
        width: 150,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    title: {
        color: "#FFFFFF",
        fontSize: 18,
        fontWeight: "200",
        position: "absolute",
        backgroundColor: "transparent",
    },
    subTitle: {
        fontWeight: "400",
        textAlign: 'center',
        margin: 10,
    }
});