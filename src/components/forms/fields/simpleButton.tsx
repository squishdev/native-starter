import React from "react";
import { View, Text, TouchableHighlight } from "react-native";

interface SimpleButtonProps {
    buttonAction: () => void;
    buttonText: string;
}

export const SimpleButton = (props: SimpleButtonProps) => {
    return (
        <View>
            <TouchableHighlight onPress={ () => props.buttonAction }>
                <Text>
                    {props.buttonText}
                </Text>
            </TouchableHighlight>
        </View>
    );
}