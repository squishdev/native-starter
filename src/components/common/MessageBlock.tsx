import React, { Component } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';

export const MessageBlock = (props: any) => {
    return (
        <View style={styles.container}>
            {props.children}
        </View>
    );
}


const styles: any = StyleSheet.create({
    container: {
        backgroundColor: '#E67E22',
        borderRadius: 5,
        flex: 1,
        justifyContent: 'center',
    },
    innerContainer: {
        backgroundColor: "#B35400",
        height: 50,
        width: 150,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    title: {
        color: "#FFFFFF",
        fontSize: 18,
        fontWeight: "200",
        position: "absolute",
        backgroundColor: "transparent",
    },
    subTitle: {
        fontWeight: "400",
        textAlign: 'center',
        margin: 10,
    }
});