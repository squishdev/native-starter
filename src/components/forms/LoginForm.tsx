import React from "react";
import { View } from "react-native";
import { SimpleTextBox } from "./fields/SimpleTextBox";
import { SimpleButton } from "./fields/SimpleButton";

interface LoginFormProps {
    theSubmitAction: () => void;
}

export const LoginForm = (props: LoginFormProps) => {
    return (
        <View>
            <SimpleTextBox isSecure={ false } placeholder="Login" />
            <SimpleTextBox isSecure={ true } placeholder="Password" />
            <SimpleButton buttonText="Login" buttonAction={ props.theSubmitAction } />
        </View>
    );
}