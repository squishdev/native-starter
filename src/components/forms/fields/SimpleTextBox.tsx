import React from 'react';
import {TextInput} from "react-native";

interface SimpleTextBoxProps {
    isSecure: boolean;
    placeholder: string;
}

export const SimpleTextBox = (props: SimpleTextBoxProps) => {
    return (
        <TextInput secureTextEntry={ props.isSecure } style={this.styles.forTextBox}
                   placeholder={ props.placeholder } />
    )
}

const styles: any = {
    forTextBox: {
        height: 30
    }
}