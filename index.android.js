/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { MainApp } from "./build/MainApp";


AppRegistry.registerComponent('swipeCore', () => MainApp);
